// SPDX-FileCopyrightText: 2021 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

import tap from "tap";
import any from '../lib/any.mjs';

const haystack = new Array('foo', 'bar', 'baz', 1, 2, 3);

tap.equal(any("foo", haystack), true, "We found our needle");
tap.equal(any("nope", haystack), false, ".. and now our needle isn't found")

tap.equal(any(1, haystack), true, "We found our needle");
tap.equal(any(4, haystack), false, ".. and now our needle isn't found")

function check_if_bar(i) {
  return i === "bar";
}
tap.equal(any(check_if_bar, haystack), true, "We found our needle via a function")

function check_if_nope(i) {
  return i === "nope";
}
tap.equal(any(check_if_nope, haystack), false, ".. and now our needle isn't found")

tap.equal(any(element => element === 'baz', haystack), true, "Arrow needle found")
tap.equal(any(element => element === 'bas', haystack), false,
  ".. and needle was not found in arrow function")
