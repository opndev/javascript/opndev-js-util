// SPDX-FileCopyrightText: 2023 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

import tap from "tap";
import range from '../lib/range.mjs';

tap.same(range(1, 8), new Array(1, 2, 3, 4, 5, 6, 7, 8), "our array is correct");
tap.same(range(0, 9, 2), new Array(0, 2, 4, 6, 8), ".. also with two steps");
tap.same(range(4, 6), new Array(4, 5, 6), ".. 4-6 is also ok");
tap.same(range(4, 8, 3), new Array(4, 7), ".. 4-8, with three steps is also ok");
