// SPDX-FileCopyrightText: 2023 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

import tap from "tap";
import pow_of_two from '../lib/powtwo.mjs';

tap.equal(pow_of_two(2), 1, '2 has 1 as the power of two');
tap.equal(pow_of_two(4), 2, '.. and also has it correct for four');
tap.equal(pow_of_two("4"), 2, '.. and also has it correct for four as string');

tap.throws(
  () => pow_of_two('a'),
  {
    name: 'Error',
    message: 'a is not a number',
  }
);
