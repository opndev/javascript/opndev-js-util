// SPDX-FileCopyrightText: 2021 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

import tap from "tap";
import { defined, any, none, pow_of_two, is_pow_of_two, range, ucFirst, lcFirst }
    from '../lib/index.mjs';

tap.type(defined, 'function',
  "defined() is exported by index.mjs")

tap.type(any, 'function',
  "any() is exported by index.mjs")

tap.type(none, 'function',
  "none() is exported by index.mjs")

tap.type(pow_of_two, 'function',
  "pow_of_two() is exported by index.mjs")

tap.type(is_pow_of_two, 'function',
  "is_pow_of_two() is exported by index.mjs")

tap.type(range, 'function',
  "range() is exported by index.mjs")

tap.type(ucFirst, 'function',
  "range() is exported by index.mjs")

tap.type(lcFirst, 'function',
  "range() is exported by index.mjs")
