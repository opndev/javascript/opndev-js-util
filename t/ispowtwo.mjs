// SPDX-FileCopyrightText: 2023 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

import tap from "tap";
import is_pow_of_two from '../lib/ispowtwo.mjs';

tap.equal(is_pow_of_two(2), true, '2 is a power of two');
tap.equal(is_pow_of_two(4), true, '.. and so is 4');
tap.equal(is_pow_of_two(5), false, '.. but not 5');

tap.throws(
  () => is_pow_of_two('a'),
  {
    name: 'Error',
    message: 'a is not a number',
  }
);
