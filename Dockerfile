# SPDX-FileCopyrightText: 2021 - 2024 Wesley Schwengle <wesley@opndev.io>
#
# SPDX-License-Identifier: MIT

FROM registry.gitlab.com/opndev/javascript/docker:latest as base

COPY package.json package.json
RUN ./dev-bin/docker-npm

COPY . .
RUN npm test
RUN npm run jsdoc

FROM scratch as pages
COPY --from=base /src/app/out /

