// SPDX-FileCopyrightText: 2023 - 2024 Wesley Schwengle <wesley@opndev.io>
//
// SPDX-License-Identifier: MIT

/**
 * Return a range
 * @function range
 * @param start {Int}
 * @param stop {Int}
 * @returns {Array} An array with the requested values
 */

export default function range(start, stop, step = 1) {
  return Array.from({
    length: (stop - start) / step + 1
  },
  (_, i) => start + i * step
  );
}

export {
  range
};
